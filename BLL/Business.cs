﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DLL;
using BusinessObjects;

namespace BLL
{
    public class Business
    {
		DAL dal = new DAL();
		public bool Login(string usr,string pwd,int login_type)
		{
			dal.InitializeDAL();
			if (login_type == 2)
			{
				//means the password given by user is to be matched against the encrypted one which is stored in DB
				//therefore weapply encryption on the passw entered by user now to see whether it is correct against the actual pass
				string encryptedPass = encryptPassword(pwd);
				return dal.LoginAdmin(usr, encryptedPass, login_type);
			}
			return dal.LoginAdmin(usr, pwd,login_type);
		}

		public bool registerUser(int uid, string usr, string pass)
		{
			//encrypt the passowrd entered by user
			dal.InitializeDAL();
			string encryptedPass = encryptPassword(pass);
			if (dal.registerUser(uid, usr, encryptedPass))
			{
				return true;
			}
			return false;
		}

		public bool changePassword(int id,string pass)
		{
			string encryptedPass = encryptPassword(pass);
			if (dal.changePassword(id, encryptedPass))
			{
				return true;
			}
			else
				return false;
		}

		public string encryptPassword(string pass)
		{
			int len = pass.Length;
			int  endIndex = len - 1;
			StringBuilder encrypted = new StringBuilder(pass);  //we are going to overwrite encrypted string's value
			if (len % 2 == 0)
			{
				//even length
				for (int writer = 0, reader = 0; writer < len && reader <= len - 1 / 2; writer += 2)
				{
					if (writer % 2 == 0)
					{
						//place 1st half's character here
						encrypted[writer] = pass[reader++];
					}
				}
				for (int writer = 1, reader = endIndex; writer < len && reader > 0; writer += 2)
				{
					if (writer % 2 == 1)
					{
						//place 1st half's character here
						encrypted[writer] = pass[reader--];
					}
				}
			}
			if (len % 2 == 1)
			{
				//odd length pass
				//	ABC12
				//  A2B1C
				int centre_odd = (int)Math.Ceiling(len / 2.0);
				Console.WriteLine(centre_odd);
				for (int writer = 0, reader = 0; writer < len && reader < centre_odd; writer += 2)
				{
					if (writer % 2 == 0)
					{
						//place 1st half's character here
						encrypted[writer] = pass[reader++];
					}
				}
				for (int writer = 1, reader = endIndex; writer < len && reader > centre_odd; writer += 2)
				{
					if (writer % 2 == 1)
					{
						//place 1st half's character here
						encrypted[writer] = pass[reader--];
					}
				}
			}

			return encrypted.ToString();
		}


		public bool addEmployee(Employee emp)
		{
			if (dal.addEmployee(emp))
			{
				return true;
			}else
			{
				return false;
			}
		}

		public bool editEmployeeRecord(int eid, string name,Double sal,Double br,Double tax,string desig)
		{
			if (dal.editEmpRec(eid, name, sal, br, tax, desig))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public Employee searchEmployee(int id)
		{
			return dal.searchEmp(id);
		}

		public bool deleteEmployee(int eid)
		{
			if (dal.deleteEmpRec(eid))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		static void Main(string[] args)
		{

		}
	}
}
