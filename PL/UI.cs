﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL;
using BusinessObjects;

namespace PL
{
    public class UI
    {
		Business businessLogicLayer = new Business();
		bool isAdmin=false;
        static void Main(string[] args)
        {

        }

		public void displayMainMenu()
		{
			Console.WriteLine("\n\t\t\t--- Welcome to Employee Management System ---");
			Console.WriteLine("\t\t\t--- Press 1 for Admin ---");
			Console.WriteLine("\t\t\t--- Press 2 for User---");
			Console.WriteLine("\t\t\t--- Press 3 To Register as a User---");
			Console.Write("\n\t\t\tEnter Choice");
		}

		public void handleMainMenuChoice()
		{
			string choice = Console.ReadLine();
			while (!(choice.Equals("1") || choice.Equals("2") || choice.Equals("3")))
			{
				Console.WriteLine("\t\tWrong Choice");
				Console.Write("\t\tEnter Choice: ");
				choice = Console.ReadLine();
			}
			//Console.WriteLine("\t\t\tChoice is " + choice);
			switch (choice)
			{
				case "1":
					{
						Console.Clear();
						Console.WriteLine("\n\t\t\t--- Welcome to Employee Management System ---");
						Console.WriteLine("\n\t\t\t\t--- ADMIN PANEL ---");
						loginAdmin();
					}
					break;
				case "2":
					{
						Console.Clear();
						Console.WriteLine("\n\t\t\t--- Welcome to Employee Management System ---");
						Console.WriteLine("\n\t\t\t\t--- USER PANEL ---");
						loginUser();
					}
					break;
				case "3":
					{
						Console.Clear();
						Console.WriteLine("\n\t\t\t--- Welcome to Employee Management System ---");
						Console.WriteLine("\n\t\t\t\t--- User Registration ---\n");
						registerUser();

						displayMainMenu();
						handleMainMenuChoice();

					}
					break;
				default:
					{

					}
					break;
			}
		}

		public void loginAdmin()
		{
			Console.Write("\t\t\tEnter Username : ");
			string username = Console.ReadLine();
			Console.Write("\t\t\tEnter Password : ");
			string password = Console.ReadLine();
			//try to login as admin
			if (businessLogicLayer.Login(username, password, 1))
			{
				Console.WriteLine("***Admin logged in!***");
				isAdmin = true;
				adminFlow();
			}
		}

		public void loginUser()
		{
			//try to login as user
			Console.Write("\t\t\tEnter Username : ");
			string username = Console.ReadLine();
			Console.Write("\t\t\tEnter Password : ");
			string password = Console.ReadLine();
			if (businessLogicLayer.Login(username, password, 2))
			{
				Console.WriteLine("***User logged in!***");
				manageEmployees();
			}
		}

        public void StartUI()
        {
			while (true)
			{
				displayMainMenu();
				handleMainMenuChoice();
			}
        }

		private void registerUser()
		{
			Console.Write("\t\t\t\tEnter UID : ");
			string uid_str = Console.ReadLine();
			int uid = Convert.ToInt32(uid_str);
			Console.Write("\t\t\t\tEnter Username : ");
			string usr = Console.ReadLine();

			Console.Write("\t\t\t\tEnter Password : ");
			string pwd1 = Console.ReadLine();
			Console.Write("\t\t\t\tEnter Password Again: ");
			string pwd2 = Console.ReadLine();
			while (pwd1!=pwd2)
			{
				Console.WriteLine("\t\t\tPasswords do not match!");
				Console.Write("\t\t\t\tEnter Password : ");
				pwd1 = Console.ReadLine();
				Console.Write("\t\t\t\tEnter Password Again: ");
				pwd2 = Console.ReadLine();
			}
			if(businessLogicLayer.registerUser(uid, usr, pwd1))
			{
				Console.Write("\t\t\t\t***User Registered Successfully!***\n");
			}else
			{
				Console.Write("\t\t\t\t***User Could Not Be Registered.***\n");
			}
		}

		public void displayAdminUI()
		{
			Console.WriteLine("\n\t\t\tPress 1 for Employee Management");
			Console.WriteLine("\t\t\tPress 2 to Change Password of Users");
		}

		public void adminFlow()
		{
			displayAdminUI();
			string adminChoice = "";
			while (!(adminChoice == "1" || adminChoice == "2"))
			{
				Console.Write("\t\t\tEnter Choice");
				adminChoice = Console.ReadLine();
			}
			if (adminChoice == "1")
			{
				//let the admin manage the employees
				manageEmployees();
			}
			else
			{
				//change user password by id
				Console.Write("\t\t\tEnter User ID : ");
				string id_str = Console.ReadLine();
				int id = Convert.ToInt32(id_str);
				Console.Write("\t\t\tEnter New Password : ");
				string pass = Console.ReadLine();
				if(businessLogicLayer.changePassword(id, pass))
				{
					Console.Write("\t\t\t***Password Updated Successfully!***");
					adminFlow();
				}
				else
				{
					Console.Write("\t\t\t***No updates were made!***");
					adminFlow();
				}
			}
		}

		public void addAnEmployee()
		{
			//call add employee
			Console.Write("\t\t\tEnter Employee ID : ");
			string eid_str = Console.ReadLine();
			int eid = Convert.ToInt32(eid_str);
			Console.Write("\t\t\tEnter name : ");
			string name = Console.ReadLine();
			Console.Write("\t\t\tEnter Designation : ");
			string desig = Console.ReadLine();
			Console.Write("\t\t\tEnter Salary : ");
			string salary_str = Console.ReadLine();
			Double salary = Convert.ToDouble(salary_str);
			Double br=0, tx=0;
			//Console.Write("\t\t\tEnter Bonus Ratio : ");
			//string br_str = Console.ReadLine();
			//br = Convert.ToDouble(br_str);
			//Console.Write("\t\t\tEnter Tax : ");
			//string tx_str = Console.ReadLine();
			//tx = Convert.ToDouble(tx_str);

			if(desig.Equals("HR") || desig.Equals("hr") || desig.Equals("Hr"))
			{
				br = 20;
				tx = 10;
			}else if(desig.Equals("Developer") || desig.Equals("developer") || desig.Equals("DEVELOPER"))
			{
				br = 50;
				tx = 12;
			}
			else if (desig.Equals("Manager") || desig.Equals("manager") || desig.Equals("MANAGER"))
			{
				br = 10;
				tx = 15;
			}

			//confirm whether to add or to cancel
			Console.WriteLine("\t\t\tPress A to Save or C to Cancel\n");
			Console.Write("\t\t\tEnter Choice : ");
			string response = Console.ReadLine();
			if (response.Equals("a") || response.Equals("A"))
			{
				//now this employee
				//create business object of Employee class and pass it to BL layer
				Employee emp = new Employee();
				emp.EID1 = eid; emp.Name1 = name; emp.Salary1 = salary; emp.BonusRatio1 = br; emp.Designation1 = desig;
				if (businessLogicLayer.addEmployee(emp))
				{
					Console.Write("\t\t\t***Successfully Added New Employee***");
					manageEmployees();
				}
				else
				{
					Console.Write("\t\t\t***Could Not Add New Employee***");
					manageEmployees();
				}
			}
			else
			{
				//cancel adding this employee 
				Console.WriteLine("\t\t\tPress * for Main Menu or C to Continue");
				if (Console.ReadLine() == "*")
				{
					//display main menu according to user type
					if (isAdmin)
					{
						adminFlow();
					}else
					{
						manageEmployees();
					}
					
				}
				else
				{
					//continue with Employee Management menu
					manageEmployees();
				}
			}

		}

		public void deleteAnEmployee()
		{
			Console.Write("\t\t\tEnter Employee ID : ");
			string eid_str = Console.ReadLine();
			int eid = Convert.ToInt32(eid_str);
			if (businessLogicLayer.deleteEmployee(eid))
			{
				Console.Write("\t\t\t***Successfully Deleted the Employee***");
				manageEmployees();
			}else
			{
				Console.Write("\t\t\t***No employee record was deleted.***");
				manageEmployees();
			}
		}

		private void manageEmployees()
		{
			displayEmployeeManagementUI();
			string empMgmtChoice = "";
			while (!(empMgmtChoice == "1" ||
					empMgmtChoice == "2" ||
					empMgmtChoice == "3" ||
					empMgmtChoice == "4"))
			{
				Console.Write("\n\t\t\tEnter Emp Mgmt Choice : ");
				empMgmtChoice = Console.ReadLine();
				//Console.WriteLine("\t\t\tGot you");
			}
			if (empMgmtChoice == "1")
			{
				addAnEmployee();
			}
			else if (empMgmtChoice == "2")
			{
				//edit employee info by id
				Console.Write("\n\t\t\tEnter Employee ID: ");
				string id_str = Console.ReadLine();
				int id = Convert.ToInt32(id_str);
				editEmployeeRec(id);
			}
			else if (empMgmtChoice == "3")
			{
				//delete employee by id
				deleteAnEmployee();
			}
			else if (empMgmtChoice == "4")
			{
				//search employee by id
				Console.Write("\t\t\tEnter Employee ID : ");
				string id_str = Console.ReadLine();
				int id = Convert.ToInt32(id_str);
				Employee searchedEmployee=businessLogicLayer.searchEmployee(id);
				if (searchedEmployee != null)
				{
					Console.WriteLine("");
					Console.WriteLine("\t\t\tEmployee Found!");
					Console.WriteLine("\t\t\tEID : " + searchedEmployee.EID1);
					Console.WriteLine("\t\t\tName : " + searchedEmployee.Name1);
					Console.WriteLine("\t\t\tDesignation : " + searchedEmployee.Designation1);
					Console.WriteLine("\t\t\tSalary : " + searchedEmployee.Salary1);
					Console.WriteLine("\t\t\tBonusRatio : " + searchedEmployee.BonusRatio1);
					Console.WriteLine("\t\t\tTax : " + searchedEmployee.Tax1);
					Console.WriteLine("");
					//ask user to edit it or not
					Console.Write("\t\t\tDo you want to edit this employee record? y/n : ");
					string op=Console.ReadLine();
					if (op.Equals("y") || op.Equals("Y"))
					{
						editEmployeeRec(searchedEmployee.EID1);
					}else
					{
						manageEmployees();
					}
					
				}
				else
				{
					//emp with this id does not exist
					Console.WriteLine("\t\t\t***Employee with this ID does not exist!***");
					manageEmployees();
				}
			}
		}

		public void editEmployeeRec(int eid)
		{
			//Console.Write("\t\t\tEnter Employee ID : ");
			//string eid_str = Console.ReadLine();
			//int eid = Convert.ToInt32(eid_str);
			Console.Write("\t\t\tEnter name : ");
			string name = Console.ReadLine();
			Console.Write("\t\t\tEnter Designation : ");
			string desig = Console.ReadLine();
			Console.Write("\t\t\tEnter Salary : ");
			string salary_str = Console.ReadLine();
			Double salary = Convert.ToDouble(salary_str);
			Double br=0, tx=0;
			if (desig.Equals("Hr") || desig.Equals("hr") || desig.Equals("HR"))
			{
				br = 20;
				tx = 10;
			}
			else if (desig.Equals("Developer") || desig.Equals("developer") || desig.Equals("DEVELOPER"))
			{
				br = 50;
				tx = 12;
			}
			else if (desig.Equals("Manager") || desig.Equals("manager") || desig.Equals("MANAGER"))
			{
				br = 10;
				tx = 15;
			}

			if (name.Equals("") && desig.Equals(""))
			{
				Console.WriteLine("\t\t\tEmployee Name and Designation not provided. No updates were made to the employee's record.");
			} else
			{
				if (businessLogicLayer.editEmployeeRecord(eid, name, salary, br, tx, desig))
				{
					Console.WriteLine("\t\t\t***Employee Record updated successfully!***");
					manageEmployees();
				}else
				{
					Console.WriteLine("\t\t\t***Employee with ID: " + eid+ " doesn't exist!***");
					manageEmployees();
				}
			}
		}

		public void displayEmployeeManagementUI()
		{
			//Console.Clear();
			Console.WriteLine();
			Console.WriteLine("\t\t\tPress 1 To Add Employee");
			Console.WriteLine("\t\t\tPress 2 To Edit Employee Info by ID");
			Console.WriteLine("\t\t\tPress 3 To Delete Employee by ID");
			Console.WriteLine("\t\t\tPress 4 To Search Employee by ID");
		}
    }
}
