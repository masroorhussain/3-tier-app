﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using BusinessObjects;

namespace DLL
{
	public class DAL
    {
		SqlConnection connection;
		string connection_string= @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=G:\Semester-9\Web Programming\Assignments\Assignment1\EMS\DLL\EMSDB.mdf;Integrated Security=True";

		public bool InitializeDAL()
		{
			if (connection==null)
			{
				connection = new SqlConnection(connection_string);
				if (connection != null)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return true;
			}
		}


		public bool registerUser(int Uid, string usr, string pass)
		{
			if (!(connection.State == System.Data.ConnectionState.Open))
			{
				connection.Open();
			}
			string query = "INSERT INTO Users (UID,Username,PassWord) VALUES (@uid,@usr,@pwd)";
			SqlCommand cmd = new SqlCommand(query, connection);
			//Console.Write("||"+cmd.CommandText+"||");
			SqlParameter p1 = new SqlParameter("uid", Uid);
			SqlParameter p2 = new SqlParameter("usr", usr);
			SqlParameter p3 = new SqlParameter("pwd", pass);
			cmd.Parameters.Add(p1);
			cmd.Parameters.Add(p2);
			cmd.Parameters.Add(p3);
			int n = cmd.ExecuteNonQuery();
			if (n > 0)
			{
				//user added successfully
				return true;
			}
			return false;
		}


		public bool LoginAdmin(string usr,string pwd,int login_type)
		{
			string query = "SELECT * FROM Admin WHERE Username=@UN AND Password=@PWD";
			if (login_type == 2)
			{
				query = "SELECT * FROM Users WHERE Username=@UN AND PassWord=@PWD";
			}
			
			SqlCommand command = new SqlCommand(query, connection);
			SqlParameter param1 = new SqlParameter("UN", usr);
			SqlParameter param2 = new SqlParameter("PWD", pwd);

			command.Parameters.Add(param1);
			command.Parameters.Add(param2);
			if (!(connection.State == System.Data.ConnectionState.Open))
			{
				connection.Open();
			}
			SqlDataReader reader = command.ExecuteReader();
			while (reader.Read())
			{
				reader.Close();
				return true;
			}
			reader.Close();
			return false;
		}

		public bool addEmployee(Employee emp)
		{
			string query = "SELECT * FROM Employee WHERE EID=@id";
			SqlCommand cmd = new SqlCommand(query, connection);
			SqlParameter p = new SqlParameter("id", emp.EID1);
			cmd.Parameters.Add(p);
			//connection.Open();	//no need to open connection again, its opened at login time
			SqlDataReader reader = cmd.ExecuteReader();
			if (reader.HasRows)
			{
				//employee with this id exists already
				// send back false
				reader.Close();
				return false;
			}
			else
			{
				reader.Close();
				string insertEmpQuery = "INSERT INTO Employee (EID,Name,Salary,BonusRatio,Tax,Designation) VALUES (@id,@name,@sal,@br,@tx,@desig)";
				SqlCommand cmd2 = new SqlCommand(insertEmpQuery, connection);
				SqlParameter p1 = new SqlParameter("id", emp.EID1);
				SqlParameter p2 = new SqlParameter("name", emp.Name1);
				SqlParameter p3 = new SqlParameter("sal", emp.Salary1);
				SqlParameter p4 = new SqlParameter("br", emp.BonusRatio1);
				SqlParameter p5 = new SqlParameter("tx", emp.Tax1);
				SqlParameter p6 = new SqlParameter("desig", emp.Designation1);
				cmd2.Parameters.Add(p1);
				cmd2.Parameters.Add(p2);
				cmd2.Parameters.Add(p3);
				cmd2.Parameters.Add(p4);
				cmd2.Parameters.Add(p5);
				cmd2.Parameters.Add(p6);
				int rows=cmd2.ExecuteNonQuery();
				if (rows == 1)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		public bool deleteEmpRec(int id)
		{
			string query = "DELETE FROM Employee WHERE EID=@eid";
			SqlCommand cmd = new SqlCommand(query, connection);
			SqlParameter p = new SqlParameter("eid", id);
			cmd.Parameters.Add(p);
			int rows=cmd.ExecuteNonQuery();
			if (rows == 1)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public bool changePassword(int id, string pass)
		{
			string query = "UPDATE Users SET PassWord=@pass WHERE UID=@uid";
			SqlCommand cmd = new SqlCommand(query, connection);
			SqlParameter p1 = new SqlParameter("uid", id);
			SqlParameter p2 = new SqlParameter("pass", pass);
			cmd.Parameters.Add(p1);
			cmd.Parameters.Add(p2);
			int n = cmd.ExecuteNonQuery();
			if (n > 0)
			{
				return true;
			}
			else
				return false;
		}

		public bool editEmpRec(int eid, string name, Double sal, Double br, Double tax, string desig)
		{
			string query = "UPDATE Employee SET Name=@name,Salary=@salary,BonusRatio=@br,Tax=@tx,Designation=@desig WHERE EID=@eid";
			SqlCommand cmd = new SqlCommand(query, connection);
			SqlParameter p = new SqlParameter("name", name);
			SqlParameter p1 = new SqlParameter("eid", eid);
			SqlParameter p2 = new SqlParameter("salary", sal);
			SqlParameter p3 = new SqlParameter("br", br);
			SqlParameter p4 = new SqlParameter("tx", tax);
			SqlParameter p5 = new SqlParameter("desig", desig);

			cmd.Parameters.Add(p); cmd.Parameters.Add(p1); cmd.Parameters.Add(p2); cmd.Parameters.Add(p3); cmd.Parameters.Add(p4);
			cmd.Parameters.Add(p5);

			int rows = cmd.ExecuteNonQuery();
			if (rows == 1)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public Employee searchEmp(int id)
		{
			string query = "SELECT * FROM Employee WHERE EID=@eid";
			SqlCommand cmdd = new SqlCommand(query, connection);
			SqlParameter p = new SqlParameter("eid", id);
			cmdd.Parameters.Add(p);
			SqlDataReader data = cmdd.ExecuteReader();
			if (data.HasRows)
			{
				data.Read();
				Employee emp = new Employee();
				emp.EID1 = (int)data["EID"];
				emp.Name1 = (string)data["Name"];
				emp.Salary1 = (Double)data["Salary"];
				emp.BonusRatio1 = (Double)data["BonusRatio"];
				emp.Tax1 = (Double)data["Tax"];
				emp.Designation1 = (string)data["Designation"];
				data.Close();
				return emp;
			}
			else
			{
				data.Close();
				return null;
			}
		}

		static void Main(string[] args)
		{

		}
	}
}
