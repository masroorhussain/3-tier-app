﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
	public class Employee
	{
		int EID;
		string Name, Designation;
		Double Salary, BonusRatio, Tax;

		public double BonusRatio1
		{
			get
			{
				return BonusRatio;
			}

			set
			{
				BonusRatio = value;
			}
		}

		public string Designation1
		{
			get
			{
				return Designation;
			}

			set
			{
				Designation = value;
			}
		}

		public int EID1
		{
			get
			{
				return EID;
			}

			set
			{
				EID = value;
			}
		}

		public string Name1
		{
			get
			{
				return Name;
			}

			set
			{
				Name = value;
			}
		}

		public double Salary1
		{
			get
			{
				return Salary;
			}

			set
			{
				Salary = value;
			}
		}

		public double Tax1
		{
			get
			{
				return Tax;
			}

			set
			{
				Tax = value;
			}
		}

		static void Main(string[] args)
		{

		}

	}
}
